# NSF LaTeX Class
## An NSF-Style Latex Class for Proposal Writing

##### Contents:
 - `./nsf.cls` is the main LaTeX class file.
 - `example.tex` is an example "Project Description" document which uses the class
 - `example.pdf` is the result of pdfTex running the example code.

##### In-Progress:
- Implement a class option for `[jam]` mode, where spacing is reduced to a minimum.
- Implement a class option for `[loose]` mode, where spacing is loosened.

##### Example:
![example](example-snap.png)