% !TeX root = example.tex

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{nsf}[2021/02/24 NSF LaTeX class]

\LoadClass[usegeometry,%
letterpaper,%
fontsize=11,%
titlepage=no,%
footheight=0in,%
%headings=standardclasses,%
chapterprefix=false%
]{scrreprt}

%\setkomafont{sectioning}{\bfseries}


\RequirePackage[left=1in,top=1in,right=1.05in,bottom=1in,nohead,nofoot]{geometry}
\RequirePackage[protrusion=false,expansion=true,kerning=true,babel=true,final]{microtype}


\RequirePackage[USenglish]{babel}
\RequirePackage{csquotes}
\RequirePackage{enumitem}
\RequirePackage{booktabs}
\RequirePackage{float}
\RequirePackage{hyperref}
\RequirePackage{url}


%  Do not reset Figure and Table counters in different sections
\RequirePackage{chngcntr}
\counterwithout{figure}{section}
\counterwithout{table}{section}

%  Do not include page numbers
\pagestyle{empty}



%\usepackage{etoolbox}
%\makeatletter
%\preto{\@tabular}{\parskip=-6pt}
%\makeatother



%\renewcommand{\topfraction}{0.85}
%\renewcommand{\bottomfraction}{0.85}
%\renewcommand{\textfraction}{0.15}
%\renewcommand{\floatpagefraction}{0.8}
%\renewcommand{\textfraction}{0.1}
\setlength{\floatsep}{1pt plus 1pt minus 1pt}
\setlength{\textfloatsep}{1pt plus 1pt minus 1pt}
\setlength{\intextsep}{6pt plus 4pt minus 2pt}
\setlength{\abovecaptionskip}{3pt}


\setlength{\parskip}{0ex}
\setlength{\parsep}{0ex}
\setlength{\headsep}{0pt}
\setlength{\topskip}{0ex}
\setlength{\topmargin}{0cm}
\setlength{\topsep}{0pt}
\setlength{\partopsep}{0ex}

\setlist{nosep,noitemsep}
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,itemindent=0.2in,leftmargin=0cm}
\setenumerate{ noitemsep,topsep=0ex,parsep=0pt,partopsep=0pt,labelsep=1ex,leftmargin=2em}
\setdescription{noitemsep, topsep=0pt,parsep=0pt, partopsep=0pt, leftmargin=0pt, font=\normalfont\textit}


\RedeclareSectionCommand[
%runin=false,
afterindent=false,
beforeskip=0em,
afterskip=4pt]{section}

\RedeclareSectionCommand[
runin=false,
afterindent=false,
beforeskip=4pt,
afterskip=0pt]{subsection}

\RedeclareSectionCommand[
runin=true,
afterindent=false,
beforeskip=2pt,
afterskip=0.8em]{subsubsection}

\RedeclareSectionCommand[
runin=true,
afterindent=false,
beforeskip=0ex,
afterskip=0.8em]{paragraph}



\makeatletter
\renewcommand{\sectionlinesformat}[4]{%
	\ifstr{#1}{section}{%
		\parbox[t]{\linewidth}{%
			{\centering\@hangfrom{\hskip #2}{\large\sffamily\MakeUppercase{#4}}\par}%
			\kern-.75\ht\strutbox\vspace{-6pt}\rule{\linewidth}{.8pt}%
		}%
	}{%
		\@hangfrom{\hskip #2}{#4}}% 
}
\makeatother

